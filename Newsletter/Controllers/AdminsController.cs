﻿using Core.Managers;
using Microsoft.AspNetCore.Mvc;
using Newsletter.Models;
using Newsletter.Models.DTOs;
using Newsletter.Services;

namespace Newsletter.Controllers
{
    [ApiController]
    [Route("admins")]
    public class AdminsController : ControllerBase
    {
        AdminsService adminsService { get; set; }
        AuthService authService { get; set; }

        public AdminsController(AdminsService adminsService, AuthService authService)
        {
            this.adminsService = adminsService;
            this.authService = authService;
        }

        [HttpPost("login")]
        public IActionResult LogIn(AuthLoginDto credentials)
        {
            AuthStatusDto results = authService.Login(credentials);

            return Ok(results);
        }

        [HttpPost("register")]
        public IActionResult Register(AuthRegisterDto admin)
        {
            AuthStatusDto results = authService.Register(admin);

            return Ok(results);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var results = adminsService.GetAll();

            if (results == null)
                return BadRequest();

            return Ok(results);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var result = adminsService.GetById(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpPost("withFilters")]
        public IActionResult GetWithFilters(NameFilters filters)
        {
            var totalNo = 0;
            var list = adminsService.GetWithFilters(filters, ref totalNo);

            var results = new FilterResponseAdmin(totalNo, list);

            return Ok(results);
        }

        [HttpPut("changePassword/{id}")]
        public void Put([FromRoute] int id, string password)
        {
            adminsService.UpdatePassword(id, password);
        }

        [HttpPut("{id}")]
        public void Put([FromRoute] int id, [FromBody] AuthRegisterDto adminDTO)
        {
            adminsService.Update(id, adminDTO);
        }


        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            adminsService.Delete(id);
        }
    }
}
