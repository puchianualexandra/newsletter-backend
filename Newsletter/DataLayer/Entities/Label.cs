﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.DataLayer.Entities
{
    public class Label
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<CollaboratorLabel> CollaboratorLabels { get; set; }
    }
}
