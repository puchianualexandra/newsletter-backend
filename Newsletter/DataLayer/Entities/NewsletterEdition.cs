﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.DataLayer.Entities
{
    public class NewsletterEdition
    {
        public int Id { get; set; }

        public DateTime EditionDate { get; set; }

        public string Url { get; set; }

        public DateTime DateOfUpload { get; set; }

        public string Description { get; set; }
        
        public bool isCurrent { get; set; }
    }
}
