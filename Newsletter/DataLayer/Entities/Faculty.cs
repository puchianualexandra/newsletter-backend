﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.DataLayer.Entities
{
    public class Faculty
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Collaborator> Collaborators { get; set; }
    }
}
