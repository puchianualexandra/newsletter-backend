﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newsletter.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.DataLayer
{
    public class DatabaseContext :DbContext
    {
        IConfiguration configuration;

        public DatabaseContext(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseSqlServer(configuration["ConnectionStrings:Newsletter"], providerOptions => providerOptions.CommandTimeout(60));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CollaboratorLabel>().HasKey(x => new { x.CollaboratorId, x.LabelId });

            modelBuilder.Entity<CollaboratorLabel>()
            .HasOne(e => e.Collaborator)
            .WithMany(e => e.CollaboratorLabels)
            .HasForeignKey(e => e.CollaboratorId)
            .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CollaboratorLabel>()
                        .HasOne(e => e.Label)
                        .WithMany(e => e.CollaboratorLabels)
                        .HasForeignKey(e => e.LabelId)
                        .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Collaborator>()
                        .HasOne(e => e.Faculty)
                        .WithMany(c => c.Collaborators)
                        .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Article>()
                        .HasOne(e => e.Collaborator)
                        .WithMany(c => c.Articles)
                        .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<ArticleItem>()
                       .HasOne(e => e.Article)
                       .WithMany(c => c.ArticleItems)
                       .OnDelete(DeleteBehavior.SetNull);
        }

        public DbSet<Admin> Admins { get; set; }

        public DbSet<Subscriber> Subscribers { get; set; }
        
        public DbSet<NewsletterEdition> Newsletters { get; set; }
        
        public DbSet<Label> Labels { get; set; }
        
        public DbSet<Suggestion> Suggestions { get; set; }
        
        public DbSet<Collaborator> Collaborators { get; set; }
        
        public DbSet<CollaboratorLabel> CollaboratorLabels { get; set; }

        public DbSet<Faculty> Faculties { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<ArticleItem> ArticleItems { get; set; }
    }
}
