﻿using Newsletter.DataLayer.Entities;
using Newsletter.Models;
using Newsletter.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Services
{
    public class NewsletterEditionsService
    {
        NewsletterEditionsRepository newsletterEditionsRepository { get; set; }

        public NewsletterEditionsService(NewsletterEditionsRepository newsletterEditionsRepository)
        {
            this.newsletterEditionsRepository = newsletterEditionsRepository;
        }

        public List<NewsletterEdition> GetAll()
        {
            return newsletterEditionsRepository.GetAll();
        }

        public void Add(NewsletterEdition newsletterEdition)
        {
            if (newsletterEdition.isCurrent)
            {
                var currentEdition = newsletterEditionsRepository.GetCurrentEdition();
                if (currentEdition.Count() != 0)
                {
                    currentEdition[0].isCurrent = false;
                    newsletterEditionsRepository.Update(currentEdition[0]);
                }
            }
            newsletterEditionsRepository.Add(newsletterEdition);
        }

        public NewsletterEdition GetById(int id)
        {
            return newsletterEditionsRepository.GetById(id);
        }

        public List<NewsletterEdition> GetWithPagination(Pagination pagination, ref int totalNo)
        {
            var results = newsletterEditionsRepository.GetAll();

            totalNo = results.Count();

            results.Sort((item1, item2) => DateTime.Compare(item1.EditionDate, item2.EditionDate));
            results.Reverse();

            return results
                .Skip((pagination.PageNo - 1) * pagination.PageSize)
                .Take(pagination.PageSize)
                .ToList();
        }

        public List<NewsletterEdition> GetByYear(int year)
        {
            var listNewsletters = newsletterEditionsRepository.GetByYear(year);

            listNewsletters.Sort((item1, item2) => DateTime.Compare(item1.EditionDate, item2.EditionDate));
            listNewsletters.Reverse();

            return listNewsletters;
        }
        
        public List<NewsletterEdition> GetCurrentEdition()
        { 
            return newsletterEditionsRepository.GetCurrentEdition();  
        }

       
        
        public void Update(NewsletterEdition updatedNewsletterEdition)
        {
            newsletterEditionsRepository.Update(updatedNewsletterEdition);
        }
        
        public void ChangeCurrentEdition(int id)
        {
            var oldCurrentEdition = newsletterEditionsRepository.GetCurrentEdition();
            if (oldCurrentEdition.Count() != 0)
            {
                oldCurrentEdition[0].isCurrent = false;
                newsletterEditionsRepository.Update(oldCurrentEdition[0]);
            }

            var newCurrentEdition = newsletterEditionsRepository.GetById(id);
            newCurrentEdition.isCurrent = true;
            newsletterEditionsRepository.Update(newCurrentEdition);
        }

        public void Delete(int id)
        {
            var newsletterEdition = GetById(id);
            if (newsletterEdition == null) return;

            newsletterEditionsRepository.Delete(newsletterEdition);
        }
    }
}
