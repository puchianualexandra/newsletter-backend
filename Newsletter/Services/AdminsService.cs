﻿using Core.Managers;
using Newsletter.DataLayer.Entities;
using Newsletter.Models;
using Newsletter.Models.DTOs;
using Newsletter.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Services
{
    public class AdminsService
    {
        AdminsRepository adminsRepository { get; set; }
        AuthService authService { get; set; }

        public AdminsService(AdminsRepository adminsRepository, AuthService authService)
        {
            this.adminsRepository = adminsRepository;
            this.authService = authService;
        }

        public List<ResponseAdminDTO> GetAll()
        {
            var results= adminsRepository.GetAll();
            var listAdminsDTO = new List<ResponseAdminDTO>();
            foreach (var admin in results)
            {
                listAdminsDTO.Add(AdminToResponseDTO(admin));
            }

            return listAdminsDTO;
        }

        public ResponseAdminDTO GetById(int id)
        {
            var admin = adminsRepository.GetById(id);
            return AdminToResponseDTO(admin);
        }

        public List<ResponseAdminDTO> GetWithFilters(NameFilters filters, ref int totalNo)
        {
            var results = new List<Admin>();

            if (filters.Name != null && filters.Name != "")
            {
                results = adminsRepository.ContainsLastName(filters.Name);
                if (results.Count == 0)
                {
                    results = adminsRepository.ContainsFirstName(filters.Name);
                }
            }
            else
            {
                results = adminsRepository.GetAll();
            }

            results.Reverse();

            var listAdminsDTO = new List<ResponseAdminDTO>();
            foreach (var admin in results)
            {
                listAdminsDTO.Add(AdminToResponseDTO(admin));
            }

            totalNo = results.Count();

            return listAdminsDTO
                .Skip((filters.PageNo - 1) * filters.PageSize)
                .Take(filters.PageSize)
                .ToList();
        }

        private ResponseAdminDTO AdminToResponseDTO(Admin payload)
        {
            var newAdmin = new ResponseAdminDTO
            {
                Id=payload.Id,
                FirstName = payload.FirstName,
                LastName = payload.LastName,
                Email = payload.Email,
                PhoneNumber = payload.PhoneNumber,
                DateOfJoin = payload.DateOfJoin,
                Role = payload.Role,
                IsNew = payload.IsNew,
            };

            return newAdmin;
        }

        public void UpdatePassword(int id, string password)  
        {
            Admin newAdmin = adminsRepository.GetById(id);
            newAdmin.IsNew = false;
            authService.UpdatePassword(newAdmin, password);
        }

        public void Update(int id, AuthRegisterDto adminDTO)
        {
            Admin updatedAdmin = adminsRepository.GetById(id);
            ToModel(ref updatedAdmin, adminDTO);

            adminsRepository.Update(updatedAdmin);
        }

        public static void ToModel(ref Admin oldAdmin, AuthRegisterDto newAdminDTO)
        {
            oldAdmin.FirstName = newAdminDTO.FirstName;
            oldAdmin.LastName = newAdminDTO.LastName;
            oldAdmin.Email = newAdminDTO.Email;
            oldAdmin.PhoneNumber = newAdminDTO.PhoneNumber;
            oldAdmin.DateOfJoin = newAdminDTO.DateOfJoin;
            oldAdmin.Role = newAdminDTO.Role;
            oldAdmin.IsNew = newAdminDTO.IsNew;
        }
        public void Delete(int id)
        {
            var admin = adminsRepository.GetById(id);
            if (admin == null) return;

            adminsRepository.Delete(admin);
        }
    }
}
