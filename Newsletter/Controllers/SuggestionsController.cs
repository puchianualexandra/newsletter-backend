﻿using Microsoft.AspNetCore.Mvc;
using Newsletter.DataLayer.Entities;
using Newsletter.Models;
using Newsletter.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Controllers
{
    [ApiController]
    [Route("suggestions")]
    public class SuggestionsController : ControllerBase
    {
        SuggestionsService suggestionsService;

        public SuggestionsController(SuggestionsService suggestionsService)
        {
            this.suggestionsService = suggestionsService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var results = suggestionsService.GetAll();

            if (results == null)
                return BadRequest();

            return Ok(results);
        }

        [HttpGet("byId/{id}")]
        public IActionResult GetById(int id)
        {
            var result = suggestionsService.GetById(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpPost("withFilters")]
        public IActionResult GetWithFilters(NameFilters filters)
        {
            var totalNo = 0;
            var list = suggestionsService.GetWithFilters(filters, ref totalNo);

            var results = new FilterResponseSuggestion(totalNo, list);

            return Ok(results);
        }

        [HttpGet("newSuggestionsNo")]
        public IActionResult GetNewSuggestionsNo()
        {
            var results = suggestionsService.GetNewSuggestionsNo();
            return Ok(results);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Suggestion suggestion)
        {
            suggestionsService.Add(suggestion);

            return Ok();
        }

        [HttpPut("changeIsNew/{id}")]
        public void Put([FromRoute] int id)
        {
            suggestionsService.UpdateIsNew(id);
        }

        [HttpPut("changeNewSuggestions")]
        public void PutNewSuggestions()
        {
            suggestionsService.UpdateNewSuggestions();
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            suggestionsService.Delete(id);
        }
    }
}
