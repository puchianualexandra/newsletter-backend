﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Models.DTOs
{
    public class CollaboratorsDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int? FacultyId { get; set; }
        public string Function { get; set; }
        public string PhotoUrl { get; set; }
        public string Description { get; set; }
        public List<int> LabelsId { get; set; }
    }
}
