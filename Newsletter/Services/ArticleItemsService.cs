﻿using Newsletter.DataLayer.Entities;
using Newsletter.Models.DTOs;
using Newsletter.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Services
{
    public class ArticleItemsService
    {
        ArticleItemsRepository articleItemsRepository  { get; set; }

        public ArticleItemsService(ArticleItemsRepository articleItemsRepository)
        {
            this.articleItemsRepository = articleItemsRepository;
        }

        public List<ArticleItem> GeArticleItems(int articleId)
        {
            return articleItemsRepository.GetArticleItemsByArticleId(articleId);
        }

        public void Add(int articleId, List<ArticleItemsDTO> articleItemsDTO)
        {
            foreach (var articleItemDTO in articleItemsDTO)
            {
                articleItemDTO.ArticleId = articleId;
                articleItemsRepository.Add(ToModel(articleItemDTO));
            }
        }

        public static ArticleItem ToModel(ArticleItemsDTO articleItemsDTO)
        {
            ArticleItem articleItem = new ArticleItem();
            articleItem.Id = articleItemsDTO.Id;
            articleItem.Type = articleItemsDTO.Type;
            articleItem.ContentImage = articleItemsDTO.ContentImage;
            articleItem.ContentText = articleItemsDTO.ContentText;
            articleItem.ArticleId = articleItemsDTO.ArticleId;

            return articleItem;
        }

        public void Delete(int articleId)
        {
            articleItemsRepository.Delete(articleId);
        }
    }
}
