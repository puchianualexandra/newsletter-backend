﻿using Newsletter.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Models
{
    public class FilterResponseAdmin
    {
        public int TotalNo { get; set; }
        public List<ResponseAdminDTO> Admins { get; set; }

        public FilterResponseAdmin(int totalNo, List<ResponseAdminDTO> admins)
        {
            this.TotalNo = totalNo;
            this.Admins = admins;
        }
    }
}
