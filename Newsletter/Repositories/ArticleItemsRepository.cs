﻿using Newsletter.DataLayer;
using Newsletter.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Repositories
{
    public class ArticleItemsRepository
    {
        DatabaseContext databaseContext { get; set; }

        public ArticleItemsRepository(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public List<ArticleItem> GetArticleItemsByArticleId(int id)
        {
            var results = databaseContext.ArticleItems
                .Where(a => a.ArticleId == id)
                .ToList();

            return results;
        }
        public void Add(ArticleItem articleItem)
        {
            articleItem.Id = 0;
            databaseContext.ArticleItems.AddRange(articleItem);
            databaseContext.SaveChanges();
        }

        public void Delete(int articleId)
        {
            var list = databaseContext.ArticleItems.Where(x => x.ArticleId == articleId);
            databaseContext.ArticleItems.RemoveRange(list);
            databaseContext.SaveChanges();
        }
    }
}
