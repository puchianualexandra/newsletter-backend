﻿using Newsletter.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Models
{
    public class FilterResponseArticles
    {
        public int TotalNo { get; set; }
        public List<ResponseArticlesDTO> Articles { get; set; }

        public FilterResponseArticles(int totalNo, List<ResponseArticlesDTO> articles)
        {
            this.TotalNo = totalNo;
            this.Articles = articles;
        }
    }
}
