﻿using Newsletter.DataLayer;
using Newsletter.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Repositories
{
    public class SuggestionsRepository
    {
        DatabaseContext databaseContext { get; set; }

        public SuggestionsRepository(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public List<Suggestion> GetAll()
        {
            var results = databaseContext.Suggestions
                .ToList();

            return results;
        }

        public Suggestion GetById(int id)
        {
            var suggestion = databaseContext.Find<Suggestion>(id);
            return suggestion;
        }

        public List<Suggestion> ContainsFirstName(string firstName)
        {
            var results = databaseContext.Suggestions
                .Where(s => s.FirstName.Contains(firstName))
                .ToList();

            return results;
        }

        public List<Suggestion> ContainsLastName(string lastName)
        {
            var results = databaseContext.Suggestions
                .Where(s => s.LastName.Contains(lastName))
                .ToList();

            return results;
        }

        public List<Suggestion> GetNewSuggestions()
        {
            var results = databaseContext.Suggestions
                .Where(c => c.IsNew == true)
                .ToList();

            return results;
        }

        public void Add(Suggestion suggestion)
        {
            databaseContext.Suggestions.AddRange(suggestion);
            databaseContext.SaveChanges();
        }

        public void Update(Suggestion updatedSuggestion)
        {
            databaseContext.Suggestions.Update(updatedSuggestion);
            databaseContext.SaveChanges();
        }
        
        public void UpdateNewSuggestions()
        {
            var list = databaseContext.Suggestions.Where(s => s.IsNew == true);
            foreach (var suggestion in list)
            {
                suggestion.IsNew = false;
            }    
            databaseContext.SaveChanges();
        }

        public void Delete(Suggestion suggestion)
        {
            databaseContext.Remove<Suggestion>(suggestion);
            databaseContext.SaveChanges();
        }
    }
}
