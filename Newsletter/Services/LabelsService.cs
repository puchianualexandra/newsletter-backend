﻿using Newsletter.DataLayer.Entities;
using Newsletter.Models.DTOs;
using Newsletter.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Services
{
    public class LabelsService
    {
        LabelsRepository labelsRepository { get; set; }

        public LabelsService(LabelsRepository labelsRepository)
        {
            this.labelsRepository = labelsRepository;
        }

        public List<LabelsDTO> GetAll()
        {
            var listLabelsDTO = new List<LabelsDTO>();

            var labels = labelsRepository.GetAll();

            foreach (var label in labels)
            {
                listLabelsDTO.Add(ToDTO(label));
            }
            return listLabelsDTO;
        }

        public static LabelsDTO ToDTO(Label label)
        {
            LabelsDTO labelDTO = new LabelsDTO();
            labelDTO.Id = label.Id;
            labelDTO.Name = label.Name;
           
            return labelDTO;
        }

        public Label GetById(int id)
        {
            return labelsRepository.GetById(id);
        }

        public void Add(LabelsDTO labelDTO)
        {
            var label = new Label();
            label.Name = labelDTO.Name;
            labelsRepository.Add(label);
        }

        public void Update(LabelsDTO updatedLabelDTO)
        {
            var label = new Label();
            label.Id = updatedLabelDTO.Id;
            label.Name = updatedLabelDTO.Name;
            labelsRepository.Update(label);
        }

        public void Delete(int id)
        {
            var label = GetById(id);
            if (label == null) return;

            labelsRepository.Delete(label);
        }
    }
}
