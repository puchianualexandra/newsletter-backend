﻿using Newsletter.DataLayer.Entities;
using Newsletter.Models.DTOs;
using Newsletter.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Services
{
    public class FacultiesService
    {
        FacultiesRepository facultiesRepository { get; set; }

        public FacultiesService(FacultiesRepository facultiesRepository)
        {
            this.facultiesRepository = facultiesRepository;
        }

        public List<FacultiesDTO> GetAll()
        {
            var listFacultiesDTO = new List<FacultiesDTO>();

            var faculties = facultiesRepository.GetAll();

            foreach (var faculty in faculties)
            {
                listFacultiesDTO.Add(ToDTO(faculty));
            }
            return listFacultiesDTO;
        }

        public static FacultiesDTO ToDTO(Faculty faculty)
        {
            FacultiesDTO facultyDTO = new FacultiesDTO();
            facultyDTO.Id = faculty.Id;
            facultyDTO.Name = faculty.Name;

            return facultyDTO;
        }

        public Faculty GetById(int id)
        {
            return facultiesRepository.GetById(id);
        }

        public void Add(FacultiesDTO facultyDTO)
        {
            var faculty = new Faculty();
            faculty.Name = facultyDTO.Name;
            facultiesRepository.Add(faculty);
        }

        public void Update(FacultiesDTO updatedFacultyDTO)
        {
            var faculty = new Faculty();
            faculty.Id = updatedFacultyDTO.Id;
            faculty.Name = updatedFacultyDTO.Name;
            facultiesRepository.Update(faculty);
        }

        public void Delete(int id)
        {
            var faculty = GetById(id);
            if (faculty == null) return;

            facultiesRepository.Delete(faculty);
        }
    }
}
