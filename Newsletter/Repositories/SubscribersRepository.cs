﻿using Newsletter.DataLayer;
using Newsletter.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Repositories
{
    public class SubscribersRepository
    {
        DatabaseContext databaseContext { get; set; }

        public SubscribersRepository(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public List<Subscriber> GetAll()
        {
            var results = databaseContext.Subscribers
                .ToList();

            return results;
        }

        public List<Subscriber> GetByEmail(string email)
        {
            var results = databaseContext.Subscribers
                .Where(s => s.Email == email)
                .ToList();

            return results; 
        }

        public List<Subscriber> ContainsEmail(string email)
        {
            var results = databaseContext.Subscribers
                .Where(s => s.Email.Contains(email))
                .ToList();

            return results;
        }

        public Subscriber GetById(int id)
        {
            var subscriber = databaseContext.Find<Subscriber>(id);
            return subscriber;
        }
        
        public void Add(Subscriber subscriber)
        {
            databaseContext.Subscribers.AddRange(subscriber);
            databaseContext.SaveChanges();
        }

        public void Delete(Subscriber subscriber)
        {
            databaseContext.Remove<Subscriber>(subscriber);
            databaseContext.SaveChanges();            
        }

    }
}
