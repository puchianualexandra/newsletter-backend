﻿using Newsletter.DataLayer.Entities;
using Newsletter.Models;
using Newsletter.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Services
{
    public class SubscribersService
    {
        SubscribersRepository subscribersRepository { get; set; }

        public SubscribersService(SubscribersRepository subscribersRepository)
        {
            this.subscribersRepository = subscribersRepository;
        }

        public List<Subscriber> GetAll()
        {
            return subscribersRepository.GetAll();
        }
        
        public List<Subscriber> GetByEmail(string email)
        {
            return subscribersRepository.GetByEmail(email);
        }

        public List<Subscriber> GetWithFilters(SubscriberFilters filters, ref int totalNo)
        {
            var results = new List<Subscriber>();

            if (filters.Email != null && filters.Email != "")
            {
                results = subscribersRepository.ContainsEmail(filters.Email);
            }
            else
            {
                results = subscribersRepository.GetAll();
            }

            totalNo = results.Count();

            return results
                .Skip((filters.PageNo - 1) * filters.PageSize)
                .Take(filters.PageSize)
                .ToList();
        }

        public Subscriber GetById(int id)
        {
            return subscribersRepository.GetById(id);
        }

        public string Add(string email)
        {
            var subscriber = new Subscriber
            {
                Email = email,
                DateOfJoin = DateTime.Now
            };

            if ((subscribersRepository.GetByEmail(email)).Count() > 0)
            {
                return "Email already exist!";
            }
            else
            {
                subscribersRepository.Add(subscriber);
                return "Success!";
            }


        }

        public void Delete(int id)
        {
            var subscriber = GetById(id);
            if (subscriber == null) return;

            subscribersRepository.Delete(subscriber);
        }
    }
}
