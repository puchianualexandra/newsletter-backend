﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Models.DTOs
{
    public class ResponseArticlesDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime DateOfUpload { get; set; }
        public int? CollaboratorId { get; set; }
        public string Description { get; set; }
        public bool IsPosted { get; set; }
        public string SpecialSection { get; set; }
    }
}
