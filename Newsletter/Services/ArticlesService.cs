﻿using Newsletter.DataLayer.Entities;
using Newsletter.Models;
using Newsletter.Models.DTOs;
using Newsletter.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Services
{
    public class ArticlesService
    {
        ArticlesRepository articlesRepository { get; set; }
        ArticleItemsService articleItemsService { get; set; }

        public ArticlesService(ArticlesRepository articlesRepository, ArticleItemsService articleItemsService)
        {
            this.articlesRepository = articlesRepository;
            this.articleItemsService = articleItemsService;
        }

        public List<Article> GetAll()
        {
            return articlesRepository.GetAll();
        }
        
        public List<Article> GetAllPosted()
        {
            return articlesRepository.GetAllPosted();
        }

        public string Add(ArticlesDTO articlesDTO)
        {
            var article = ToModel(articlesDTO);
            articlesRepository.Add(article);
            articleItemsService.Add(article.Id, articlesDTO.ArticleItemsDTO);
            
            return "Success!";
        }

        public void Update(ArticlesDTO updatedArticlesDTO)
        {
            var articleItems = updatedArticlesDTO.ArticleItemsDTO;
            
            Article updatedArticle = ToModel(updatedArticlesDTO);

            articlesRepository.Update(updatedArticle);

            articleItemsService.Delete(updatedArticle.Id);

            if (articleItems != null)
            {
                articleItemsService.Add(updatedArticle.Id, articleItems);
            }
        }

        public static Article ToModel(ArticlesDTO articlesDTO)
        {
            Article article = new Article();
            article.Id = articlesDTO.Id;
            article.Title = articlesDTO.Title;
            article.DateOfUpload = articlesDTO.DateOfUpload;
            article.CollaboratorId = articlesDTO.CollaboratorId;
            article.Description = articlesDTO.Description;
            article.IsPosted = articlesDTO.IsPosted;
            article.SpecialSection = articlesDTO.SpecialSection;

            return article;
        }

        public static ResponseArticlesDTO ToResponseDTO(Article article)
        {
            ResponseArticlesDTO responseArticlesDTO = new ResponseArticlesDTO();
            responseArticlesDTO.Id = article.Id;
            responseArticlesDTO.Title = article.Title;
            responseArticlesDTO.DateOfUpload = article.DateOfUpload;
            responseArticlesDTO.CollaboratorId = article.CollaboratorId;
            responseArticlesDTO.Description = article.Description;
            responseArticlesDTO.IsPosted = article.IsPosted;
            responseArticlesDTO.SpecialSection = article.SpecialSection;

            return responseArticlesDTO;
        }

        public List<ResponseArticlesDTO> GetByCollaboratorId(int id)
        {
            var dbListArticles = articlesRepository.GetByCollaboratorId(id);

            var listResponseArticlesDTO = new List<ResponseArticlesDTO>();
            foreach (var article in dbListArticles)
            {
                listResponseArticlesDTO.Add(ToResponseDTO(article));
            }

            return listResponseArticlesDTO;
        }
        
        public ResponseArticlesDTO GetById(int id)
        {
            return ToResponseDTO(articlesRepository.GetById(id));
        }

        public List<ResponseArticlesDTO> GetWithFiltersUser(UserFilters filters, ref int totalNo)
        {
            if (filters.Title == null)
            {
                filters.Title = "";
            }
            
            if (filters.SpecialSection == null)
            {
                filters.SpecialSection = "";
            }

            var results = articlesRepository.ContainsFiltersUser(filters);

            results.Reverse();

            var listResultsDTO = new List<ResponseArticlesDTO>();

            foreach (var article in results)
            {
                ResponseArticlesDTO articleDTO = ToResponseDTO(article);
                listResultsDTO.Add(articleDTO);
            }

            totalNo = listResultsDTO.Count();

            return listResultsDTO
                .Skip((filters.PageNo - 1) * filters.PageSize)
                .Take(filters.PageSize)
                .ToList();
        }

        public List<ResponseArticlesDTO> GetWithFiltersAdmin(AdminFilters filters, ref int totalNo)
        {
            if (filters.Title == null)
            {
                filters.Title = "";
            }

            var results = articlesRepository.ContainsFiltersAdmin(filters);

            results.Reverse();

            var listResultsDTO = new List<ResponseArticlesDTO>();

            foreach (var article in results)
            {
                ResponseArticlesDTO articleDTO = ToResponseDTO(article);
                listResultsDTO.Add(articleDTO);
            }

            totalNo = listResultsDTO.Count();

            return listResultsDTO
                .Skip((filters.PageNo - 1) * filters.PageSize)
                .Take(filters.PageSize)
                .ToList();
        }

        public List<ArticleItem> GetArticleItems(int id)
        {
           return articleItemsService.GeArticleItems(id);
        }

        public void UpdateIsPosted(int id)
        {
            Article article = articlesRepository.GetById(id);
            article.IsPosted = !(article.IsPosted);
            articlesRepository.UpdateIsPosted(article);
        }

        public void Delete(int id)
        {
            Article article = articlesRepository.GetById(id);
            if (article == null) return;

            articleItemsService.Delete(article.Id);
            articlesRepository.Delete(article);
        }
    }
}
