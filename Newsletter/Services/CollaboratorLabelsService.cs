﻿using Newsletter.DataLayer.Entities;
using Newsletter.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Services
{
    public class CollaboratorLabelsService
    {
        CollaboratorLabelsRepository collaboratorLabelsRepository { get; set; }

        public CollaboratorLabelsService(CollaboratorLabelsRepository collaboratorLabelsRepository)
        {
            this.collaboratorLabelsRepository = collaboratorLabelsRepository;
        }

        public List<int> GetLabelsId(int collaboratorId)
        {
            var collaboratorLabels = new List<CollaboratorLabel>();
            collaboratorLabels = collaboratorLabelsRepository.GetLabelsByCollaboratorId(collaboratorId);

            var labelsId = new List<int>();
            foreach (var collaboratorLabel in collaboratorLabels)
            {
                labelsId.Add(collaboratorLabel.LabelId);
            }

            return labelsId;
        }

        public void Add(int collaboratorId, List<int> labelsId)
        {
            foreach (var labelId in labelsId)
            {
                var collaboratorLabel = new CollaboratorLabel();
                collaboratorLabel.CollaboratorId = collaboratorId;
                collaboratorLabel.LabelId = labelId;

                collaboratorLabelsRepository.Add(collaboratorLabel);
            }
        }

        public void Delete(int collaboratorId)
        {
            collaboratorLabelsRepository.Delete(collaboratorId);
        }
    }
}
