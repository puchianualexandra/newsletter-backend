﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.DataLayer.Entities
{
    public class Collaborator
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int? FacultyId { get; set; }
        public virtual Faculty Faculty { get; set; }
        public string Function { get; set; }
        public string PhotoUrl { get; set; }
        public string Description { get; set; }
        public List<CollaboratorLabel> CollaboratorLabels { get; set; }
        public List<Article> Articles { get; set; }
    }
}
