﻿using Microsoft.AspNetCore.Mvc;
using Newsletter.DataLayer.Entities;
using Newsletter.Models;
using Newsletter.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Controllers
{
    [ApiController]
    [Route("newsletterEditions")]
    public class NewsletterEditionsController : ControllerBase
    {
        NewsletterEditionsService newsletterEditionsService;

        public NewsletterEditionsController(NewsletterEditionsService newsletterEditionsService)
        {
            this.newsletterEditionsService = newsletterEditionsService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var results = newsletterEditionsService.GetAll();

            if (results == null)
                return BadRequest();

            return Ok(results);
        }

        [HttpGet("count")]
        public IActionResult Count()
        {
            var results = newsletterEditionsService.GetAll();

            return Ok(results.Count);
        }

        [HttpGet("byId/{id}")]
        public IActionResult GetById(int id)
        {
            var result = newsletterEditionsService.GetById(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpGet("{year}")]
        public IActionResult GetByYear(int year)
        {
            var result = newsletterEditionsService.GetByYear(year);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpPost("withPagination")]
        public IActionResult GetWithPagination(Pagination pagination)
        {
            var totalNo = 0;
            var list = newsletterEditionsService.GetWithPagination(pagination, ref totalNo);

            var results = new PaginationResponseNewsletter(totalNo, list);

            return Ok(results);
        }

        [HttpGet("currentEdition")]
        public IActionResult GetCurrentEdition()
        {
            var results = newsletterEditionsService.GetCurrentEdition();

            if (results == null)
                return NotFound();

            return Ok(results);
        }

        [HttpPost]
        public IActionResult Post([FromBody] NewsletterEdition newsletterEdition)
        {
            newsletterEditionsService.Add(newsletterEdition);

            return Ok();
        }

        [HttpPut("{id}")]
        public void Put([FromRoute] int id, [FromBody] NewsletterEdition newsletterEdition)
        {
            newsletterEdition.Id = id;

            newsletterEditionsService.Update(newsletterEdition);
        }

        [HttpPut("changeCurrentEdition/{id}")]
        public void Put([FromRoute] int id)
        {
            newsletterEditionsService.ChangeCurrentEdition(id);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            newsletterEditionsService.Delete(id);
        }
    }
}
