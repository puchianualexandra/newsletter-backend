﻿using Newsletter.DataLayer;
using Newsletter.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Repositories
{
    public class FacultiesRepository
    {
        DatabaseContext databaseContext { get; set; }

        public FacultiesRepository(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public List<Faculty> GetAll()
        {
            var results = databaseContext.Faculties
                .ToList();

            return results;
        }

        public Faculty GetById(int id)
        {
            var faculty = databaseContext.Find<Faculty>(id);
            return faculty;
        }

        public void Add(Faculty faculty)
        {
            databaseContext.Faculties.AddRange(faculty);
            databaseContext.SaveChanges();
        }

        public void Update(Faculty updatedFaculty)
        {
            databaseContext.Faculties.Update(updatedFaculty);
            databaseContext.SaveChanges();
        }

        public void Delete(Faculty faculty)
        {
            databaseContext.Remove<Faculty>(faculty);
            databaseContext.SaveChanges();
        }
    }
}
