﻿using Newsletter.DataLayer.Entities;
using Newsletter.Models;
using Newsletter.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Services
{
    public class SuggestionsService
    {
        SuggestionsRepository suggestionsRepository { get; set; }

        public SuggestionsService(SuggestionsRepository suggestionsRepository)
        {
            this.suggestionsRepository = suggestionsRepository;
        }

        public List<Suggestion> GetAll()
        {
            return suggestionsRepository.GetAll();
        }

        public Suggestion GetById(int id)
        {
            return suggestionsRepository.GetById(id);
        }

        public int GetNewSuggestionsNo()
        {
            return suggestionsRepository.GetNewSuggestions().Count();
        }

        public List<Suggestion> GetWithFilters(NameFilters filters, ref int totalNo)
        {
            var results = new List<Suggestion>();

            if (filters.Name != null && filters.Name != "")
            {
                results = suggestionsRepository.ContainsLastName(filters.Name);
                if (results.Count == 0)
                {
                    results = suggestionsRepository.ContainsFirstName(filters.Name);
                }
            }
            else
            {
                results = suggestionsRepository.GetAll();
            }

            results.Reverse();

            totalNo = results.Count();

            return results
                .Skip((filters.PageNo - 1) * filters.PageSize)
                .Take(filters.PageSize)
                .ToList();
        }
        public void Add(Suggestion suggestion)
        {
            suggestion.IsNew = true;
            suggestionsRepository.Add(suggestion);
        }

        public void UpdateIsNew(int id)
        {
            Suggestion suggestion = suggestionsRepository.GetById(id);
            suggestion.IsNew = false;
            suggestionsRepository.Update(suggestion);
        }
        
        public void UpdateNewSuggestions()
        {
            suggestionsRepository.UpdateNewSuggestions();
        }
        public void Delete(int id)
        {
            var suggestion = GetById(id);
            if (suggestion == null) return;

            suggestionsRepository.Delete(suggestion);
        }
    }
}
