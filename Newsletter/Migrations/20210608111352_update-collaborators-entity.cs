﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Newsletter.Migrations
{
    public partial class updatecollaboratorsentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CollaboratorLabels_Labels_LabelId",
                table: "CollaboratorLabels");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Collaborators",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_CollaboratorLabels_Labels_LabelId",
                table: "CollaboratorLabels",
                column: "LabelId",
                principalTable: "Labels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CollaboratorLabels_Labels_LabelId",
                table: "CollaboratorLabels");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Collaborators");

            migrationBuilder.AddForeignKey(
                name: "FK_CollaboratorLabels_Labels_LabelId",
                table: "CollaboratorLabels",
                column: "LabelId",
                principalTable: "Labels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
