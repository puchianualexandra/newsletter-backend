﻿using Microsoft.AspNetCore.Mvc;
using Newsletter.DataLayer.Entities;
using Newsletter.Models;
using Newsletter.Models.DTOs;
using Newsletter.Services;
using System.Linq;

namespace Newsletter.Controllers
{
    [ApiController]
    [Route("collaborators")]
    public class CollaboratorsController : ControllerBase
    {
        CollaboratorsService collaboratorsService;

        public CollaboratorsController(CollaboratorsService collaboratorsService)
        {
            this.collaboratorsService = collaboratorsService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var results = collaboratorsService.GetAll();

            if (results == null)
                return BadRequest();

            return Ok(results);
        }

        [HttpGet("count")]
        public IActionResult Count()
        {
            var results = collaboratorsService.GetAll();

            return Ok(results.Count);
        }

        [HttpPost]
        public IActionResult Post([FromBody] CollaboratorsDTO collaboratorsDTO)
        {
            var response = collaboratorsService.Add(collaboratorsDTO); 

            return Ok(new { response = response });
        }

        [HttpPut("{id}")]
        public void Put([FromRoute] int id, [FromBody] CollaboratorsDTO collaboratorsDTO)
        {
            collaboratorsDTO.Id = id;

            collaboratorsService.Update(collaboratorsDTO);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var result = collaboratorsService.GetById(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpPost("withFilters")]
        public IActionResult GetWithFilters(NameFilters filters)
        {
            var totalNo = 0;
            var list = collaboratorsService.GetWithFilters(filters, ref totalNo);

            var results = new FilterResponseCollaborator(totalNo, list);

            return Ok(results);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            collaboratorsService.Delete(id);
        }
    }

}
