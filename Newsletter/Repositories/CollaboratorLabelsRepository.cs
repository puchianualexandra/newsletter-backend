﻿using Newsletter.DataLayer;
using Newsletter.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Repositories
{
    public class CollaboratorLabelsRepository
    {
        DatabaseContext databaseContext { get; set; }

        public CollaboratorLabelsRepository(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public List<CollaboratorLabel> GetLabelsByCollaboratorId(int id)
        {
            var results = databaseContext.CollaboratorLabels
                .Where(s => s.CollaboratorId == id)
                .ToList();

            return results;
        }
        public void Add(CollaboratorLabel collaboratorLabel)
        {
            databaseContext.CollaboratorLabels.AddRange(collaboratorLabel);
            databaseContext.SaveChanges();
        }

        public void Delete(int collaboratorId)
        {
            var list = databaseContext.CollaboratorLabels.Where(x => x.CollaboratorId == collaboratorId);
            databaseContext.CollaboratorLabels.RemoveRange(list);
            databaseContext.SaveChanges();
        }

    }
}
