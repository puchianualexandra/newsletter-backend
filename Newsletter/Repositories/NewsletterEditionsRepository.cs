﻿using Newsletter.DataLayer;
using Newsletter.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Repositories
{
    public class NewsletterEditionsRepository
    {

        DatabaseContext databaseContext { get; set; }

        public NewsletterEditionsRepository(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public List<NewsletterEdition> GetAll()
        {
            var results = databaseContext.Newsletters
                .ToList();

            return results;
        }

        public NewsletterEdition GetById(int id)
        {
            var newsletterEdition = databaseContext.Find<NewsletterEdition>(id);
            return newsletterEdition;
        }

        public List<NewsletterEdition> GetByYear(int year)
        {
            var results = databaseContext.Newsletters
                .Where(s => s.EditionDate.Year == year)
                .ToList();

            return results;
        }

        public List<NewsletterEdition> GetCurrentEdition()
        {
            var results = databaseContext.Newsletters
                .Where(s => s.isCurrent == true)
                .ToList();

            return results;
        }

        public void Add(NewsletterEdition newsletterEdition)
        {
            databaseContext.Newsletters.AddRange(newsletterEdition);
            databaseContext.SaveChanges();
        }

        public void Update(NewsletterEdition updatedNewsletterEdition)
        {
            databaseContext.Newsletters.Update(updatedNewsletterEdition);
            databaseContext.SaveChanges();
        }

        public void Delete(NewsletterEdition newsletterEdition)
        {
            databaseContext.Remove<NewsletterEdition>(newsletterEdition);
            databaseContext.SaveChanges();
        }
    }
}
