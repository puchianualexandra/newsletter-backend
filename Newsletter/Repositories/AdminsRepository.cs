﻿using Newsletter.DataLayer;
using Newsletter.DataLayer.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Newsletter.Repositories
{
    public class AdminsRepository
    {
        DatabaseContext databaseContext { get; set; }

        public AdminsRepository(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public List<Admin> GetAll()
        {
            var results = databaseContext.Admins
                .ToList();

            return results;
        }

        public Admin GetById(int id)
        {
            var admin = databaseContext.Find<Admin>(id);
            return admin;
        }

        public Admin FindByEmail(string email)
        {
            var results = databaseContext.Admins
                .Where(e => e.Email == email)
                .FirstOrDefault();

            return results;
        }

        public void Add(Admin admin)
        {
            databaseContext.Admins.AddRange(admin);
            databaseContext.SaveChanges();
        }

        public void Update(Admin updatedAdmin)
        {
            databaseContext.Admins.Update(updatedAdmin);
            databaseContext.SaveChanges();
        }

        public List<Admin> ContainsFirstName(string firstName)
        {
            var results = databaseContext.Admins
                .Where(s => s.FirstName.Contains(firstName))
                .ToList();

            return results;
        }

        public List<Admin> ContainsLastName(string lastName)
        {
            var results = databaseContext.Admins
                .Where(s => s.LastName.Contains(lastName))
                .ToList();

            return results;
        }

        public void Delete(Admin admin)
        {
            databaseContext.Remove<Admin>(admin);
            databaseContext.SaveChanges();
        }
    }
}
