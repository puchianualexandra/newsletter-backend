﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Models
{
    public class AdminFilters
    {
        public string Title { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
    }
}
