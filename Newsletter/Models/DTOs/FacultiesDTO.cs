﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Models.DTOs
{
    public class FacultiesDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
