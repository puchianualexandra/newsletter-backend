﻿using Microsoft.AspNetCore.Mvc;
using Newsletter.DataLayer.Entities;
using Newsletter.Models.DTOs;
using Newsletter.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Controllers
{
    [ApiController]
    [Route("faculties")]
    public class FacultiesController : ControllerBase
    {
        FacultiesService facultiesService;

        public FacultiesController(FacultiesService facultiesService)
        {
            this.facultiesService = facultiesService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var results = facultiesService.GetAll();

            if (results == null)
                return BadRequest();

            return Ok(results);
        }

        [HttpGet("byId/{id}")]
        public IActionResult GetById(int id)
        {
            var result = facultiesService.GetById(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpPost]
        public IActionResult Post([FromBody] FacultiesDTO facultiesDTO)
        {
            facultiesService.Add(facultiesDTO);

            return Ok();
        }

        [HttpPut("{id}")]
        public void Put([FromRoute] int id, [FromBody] FacultiesDTO facultiesDTO)
        {
            facultiesDTO.Id = id;

            facultiesService.Update(facultiesDTO);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            facultiesService.Delete(id);
        }
    }
}
