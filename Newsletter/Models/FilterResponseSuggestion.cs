﻿using Newsletter.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Models
{
    public class FilterResponseSuggestion
    {
        public int TotalNo { get; set; }
        public List<Suggestion> Suggestions { get; set; }

        public FilterResponseSuggestion(int totalNo, List<Suggestion> suggestions)
        {
            this.TotalNo = totalNo;
            this.Suggestions = suggestions;
        }
    }
}
