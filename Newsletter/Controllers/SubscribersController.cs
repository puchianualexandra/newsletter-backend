﻿using Microsoft.AspNetCore.Mvc;
using Newsletter.Models;
using Newsletter.Services;

namespace Newsletter.Controllers
{
    [ApiController]
    [Route("subscribers")]
    public class SubscribersController : ControllerBase
    {
        SubscribersService subscribersService;

        public SubscribersController(SubscribersService subscribersService)
        {
            this.subscribersService = subscribersService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var results = subscribersService.GetAll();

            if (results == null)
                return BadRequest();

            return Ok(results);
        }

        [HttpPost("withFilters")]
        public IActionResult GetWithFilters(SubscriberFilters filters)
        {
            var totalNo = 0;
            var list = subscribersService.GetWithFilters(filters, ref totalNo);

            var results = new FilterResponseSubscriber(totalNo, list);

            return Ok(results);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var result = subscribersService.GetById(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }


        [HttpGet("byEmail")]
        public IActionResult GetByEmail(string email)
        {
            var result = subscribersService.GetByEmail(email);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpPost]
        public IActionResult Post(string email)
        {
            var response = subscribersService.Add(email);

            return Ok(new { response= response});
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            subscribersService.Delete(id);
        }


    }
}
