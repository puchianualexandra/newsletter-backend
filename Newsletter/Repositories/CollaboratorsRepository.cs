﻿using Newsletter.DataLayer;
using Newsletter.DataLayer.Entities;
using Newsletter.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Repositories
{
    public class CollaboratorsRepository
    {
        DatabaseContext databaseContext { get; set; }

        public CollaboratorsRepository(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public List<Collaborator> GetAll()
        {
            var results = databaseContext.Collaborators
                .ToList();

            return results;
        }

        public void Add(Collaborator collaborator)
        {
            databaseContext.Collaborators.AddRange(collaborator);
            databaseContext.SaveChanges();
        }

        public void Update(Collaborator updatedCollaborator)
        {
            databaseContext.Collaborators.Update(updatedCollaborator);
            databaseContext.SaveChanges();
        }


        public List<Collaborator> GetByEmail(string email)
        {
            var results = databaseContext.Collaborators
                .Where(c => c.Email == email)
                .ToList();

            return results;
        }

        public Collaborator GetById(int id)
        {
            var collaborator = databaseContext.Find<Collaborator>(id);
            return collaborator;
        }

        public List<Collaborator> ContainsFirstName(string firstName)
        {
            var results = databaseContext.Collaborators
                .Where(s => s.FirstName.Contains(firstName))
                .ToList();

            return results;
        }

        public List<Collaborator> ContainsLastName(string lastName)
        {
            var results = databaseContext.Collaborators
                .Where(s => s.LastName.Contains(lastName))
                .ToList();

            return results;
        }

        public void Delete(Collaborator collaborator)
        {
            databaseContext.Remove<Collaborator>(collaborator);
            databaseContext.SaveChanges();
        }
    }
}
