﻿using Newsletter.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Models
{
    public class FilterResponseSubscriber
    {
        public int TotalNo { get; set; }
        public List<Subscriber> Subscribers { get; set; }

        public FilterResponseSubscriber(int totalNo, List<Subscriber> subscribers)
        {
            this.TotalNo = totalNo;
            this.Subscribers = subscribers;
        }
    }
}
