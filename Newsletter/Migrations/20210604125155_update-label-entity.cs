﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Newsletter.Migrations
{
    public partial class updatelabelentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CollaboratorLabels_Labels_LabelId",
                table: "CollaboratorLabels");

            migrationBuilder.AddForeignKey(
                name: "FK_CollaboratorLabels_Labels_LabelId",
                table: "CollaboratorLabels",
                column: "LabelId",
                principalTable: "Labels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CollaboratorLabels_Labels_LabelId",
                table: "CollaboratorLabels");

            migrationBuilder.AddForeignKey(
                name: "FK_CollaboratorLabels_Labels_LabelId",
                table: "CollaboratorLabels",
                column: "LabelId",
                principalTable: "Labels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
