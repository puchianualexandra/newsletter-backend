﻿using Microsoft.AspNetCore.Mvc;
using Newsletter.DataLayer.Entities;
using Newsletter.Models;
using Newsletter.Models.DTOs;
using Newsletter.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Controllers
{
    [ApiController]
    [Route("articles")]
    public class ArticlesController : ControllerBase
    {
        ArticlesService articlesService { get; set; }

        public ArticlesController(ArticlesService articlesService)
        {
            this.articlesService = articlesService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var results = articlesService.GetAll();

            if (results == null)
                return BadRequest();

            return Ok(results);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var result = articlesService.GetById(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpGet("count")]
        public IActionResult Count()
        {
            var results = articlesService.GetAllPosted();

            return Ok(results.Count);
        }

        [HttpGet("getByCollaboratorId/{id}")]
        public IActionResult GetByCollaboratorId(int id)
        {
            var result = articlesService.GetByCollaboratorId(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }
        
        [HttpGet("getArticleItems/{id}")]
        public IActionResult GetArticleItems(int id)
        {
            var result = articlesService.GetArticleItems(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpPost("withFiltersUser")]
        public IActionResult GetWithFiltersUser(UserFilters filters)
        {
            var totalNo = 0;
            var list = articlesService.GetWithFiltersUser(filters, ref totalNo);

            var results = new FilterResponseArticles(totalNo, list);

            return Ok(results);
        }

        [HttpPost("withFiltersAdmin")]
        public IActionResult GetWithFiltersAdmin(AdminFilters filters)
        {
            var totalNo = 0;
            var list = articlesService.GetWithFiltersAdmin(filters, ref totalNo);

            var results = new FilterResponseArticles(totalNo, list);

            return Ok(results);
        }

        [HttpPost]
        public IActionResult Post([FromBody] ArticlesDTO articlesDTO)
        {
            var response = articlesService.Add(articlesDTO);

            return Ok(new { response = response });
        }

        [HttpPut("{id}")]
        public void Put([FromRoute] int id, [FromBody] ArticlesDTO articlesDTO)
        {
            articlesDTO.Id = id;

            articlesService.Update(articlesDTO);
        }

        [HttpPut("changeIsPosted/{id}")]
        public void Put([FromRoute] int id)
        {
            articlesService.UpdateIsPosted(id);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            articlesService.Delete(id);
        }
    }
}
