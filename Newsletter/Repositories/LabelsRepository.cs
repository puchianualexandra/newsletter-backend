﻿using Newsletter.DataLayer;
using Newsletter.DataLayer.Entities;
using Newsletter.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Repositories
{
    public class LabelsRepository
    {
        DatabaseContext databaseContext { get; set; }

        public LabelsRepository(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public List<Label> GetAll()
        {
            var results = databaseContext.Labels
                .ToList();

            return results;
        }

        public Label GetById(int id)
        {
            var label = databaseContext.Find<Label>(id);
            return label;
        }

        public void Add(Label label)
        {
            databaseContext.Labels.AddRange(label);
            databaseContext.SaveChanges();
        }

        public void Update(Label updatedLabel)
        {
            databaseContext.Labels.Update(updatedLabel);
            databaseContext.SaveChanges();
        }

        public void Delete(Label label)
        {
            var collaboratorLabels = databaseContext.CollaboratorLabels.Where(c => c.LabelId == label.Id).ToList();

            if(collaboratorLabels != null)
            {
                foreach (var collaboratorLabel in collaboratorLabels)
                {
                    databaseContext.Remove<CollaboratorLabel>(collaboratorLabel);
                }
                databaseContext.SaveChanges();
            }
            databaseContext.Remove<Label>(label);
            databaseContext.SaveChanges();
        }
    }
}
