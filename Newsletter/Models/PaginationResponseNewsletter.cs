﻿using Newsletter.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Models
{
    public class PaginationResponseNewsletter
    {
        public int TotalNo { get; set; }
        public List<NewsletterEdition> NewsletterEditions { get; set; }

        public PaginationResponseNewsletter(int totalNo, List<NewsletterEdition> newsletterEditions)
        {
            this.TotalNo = totalNo;
            this.NewsletterEditions = newsletterEditions;
        }
    }
}
