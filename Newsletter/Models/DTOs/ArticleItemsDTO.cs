﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Models.DTOs
{
    public class ArticleItemsDTO
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string ContentImage { get; set; }
        public string ContentText { get; set; }
        public int? ArticleId { get; set; }
    }
}
