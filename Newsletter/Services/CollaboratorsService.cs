﻿using Newsletter.DataLayer.Entities;
using Newsletter.Models;
using Newsletter.Models.DTOs;
using Newsletter.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Newsletter.Services
{
    public class CollaboratorsService
    {
        CollaboratorsRepository collaboratorsRepository { get; set; }
        CollaboratorLabelsService collaboratorLabelsService { get; set; }
        FacultiesService facultiesService { get; set; }

        public CollaboratorsService(CollaboratorsRepository collaboratorsRepository, CollaboratorLabelsService collaboratorLabelsService, FacultiesService facultiesService)
        {
            this.collaboratorsRepository = collaboratorsRepository;
            this.collaboratorLabelsService = collaboratorLabelsService;
            this.facultiesService = facultiesService;
        }

        public List<CollaboratorsDTO> GetAll()
        {
            var listCollaboratorsDTO = new List<CollaboratorsDTO>();
            var collaborators = collaboratorsRepository.GetAll();

            foreach (var collaborator in collaborators)
            {
                var labelsId = collaboratorLabelsService.GetLabelsId(collaborator.Id);

                CollaboratorsDTO collaboratorDTO = new CollaboratorsDTO();
                collaboratorDTO = ToDTO(collaborator);

                collaboratorDTO.LabelsId = labelsId;

                listCollaboratorsDTO.Add(collaboratorDTO);
            }

            return listCollaboratorsDTO;
        }

        public string Add(CollaboratorsDTO collaboratorDTO)
        {
            if ((collaboratorsRepository.GetByEmail(collaboratorDTO.Email)).Count() > 0)
            {
                return "Email already exist!";
            }
            else
            {
                Collaborator collaborator = ToModel(collaboratorDTO);

                collaboratorsRepository.Add(collaborator);

                var labelsId = collaboratorDTO.LabelsId;

                if (labelsId != null)
                {
                    collaboratorLabelsService.Add(collaborator.Id, labelsId);
                }
                return "Success!";
            }
        }

        public void Update(CollaboratorsDTO updatedCollaboratorDTO)
        {
            Collaborator updatedCollaborator = ToModel(updatedCollaboratorDTO);

            var labelsId = updatedCollaboratorDTO.LabelsId;

            collaboratorsRepository.Update(updatedCollaborator);

            collaboratorLabelsService.Delete(updatedCollaborator.Id);

            if (labelsId != null)
            {
                collaboratorLabelsService.Add(updatedCollaborator.Id, labelsId);
            }
        }

        public static Collaborator ToModel(CollaboratorsDTO collaboratorDTO)
        {
            Collaborator collaborator = new Collaborator();
            collaborator.Id = collaboratorDTO.Id;
            collaborator.FirstName = collaboratorDTO.FirstName;
            collaborator.LastName = collaboratorDTO.LastName;
            collaborator.Email = collaboratorDTO.Email;
            collaborator.PhoneNumber = collaboratorDTO.PhoneNumber;
            collaborator.FacultyId = collaboratorDTO.FacultyId;
            collaborator.Function = collaboratorDTO.Function;
            collaborator.PhotoUrl = collaboratorDTO.PhotoUrl;
            collaborator.Description = collaboratorDTO.Description;

            return collaborator;
        }

        public static CollaboratorsDTO ToDTO(Collaborator collaborator)
        {
            CollaboratorsDTO collaboratorDTO = new CollaboratorsDTO();
            collaboratorDTO.Id = collaborator.Id;
            collaboratorDTO.FirstName = collaborator.FirstName;
            collaboratorDTO.LastName = collaborator.LastName;
            collaboratorDTO.Email = collaborator.Email;
            collaboratorDTO.PhoneNumber = collaborator.PhoneNumber;
            collaboratorDTO.FacultyId = collaborator.FacultyId;
            collaboratorDTO.Function = collaborator.Function;
            collaboratorDTO.PhotoUrl = collaborator.PhotoUrl;
            collaboratorDTO.Description = collaborator.Description;

            return collaboratorDTO;
        }

        public CollaboratorsDTO GetById(int id)
        {
            var collaborator = collaboratorsRepository.GetById(id);
            CollaboratorsDTO collaboratorDTO = new CollaboratorsDTO();

            if (collaborator != null)
            {
                collaboratorDTO = ToDTO(collaborator);
                collaboratorDTO.LabelsId = collaboratorLabelsService.GetLabelsId(id);
            }

            return collaboratorDTO;

        }

        public List<CollaboratorsDTO> GetWithFilters(NameFilters filters, ref int totalNo)
        {
            List<Collaborator> results;

            if (filters.Name != null && filters.Name != "")
            {
                results = collaboratorsRepository.ContainsLastName(filters.Name);
                if (results.Count == 0)
                {
                    results = collaboratorsRepository.ContainsFirstName(filters.Name);
                }
            }
            else
            {
                results = collaboratorsRepository.GetAll();
            }

            results.Reverse();

            var listCollaboratorsDTO = new List<CollaboratorsDTO>();
            foreach (var collaborator in results)
            {
                var labelsId = collaboratorLabelsService.GetLabelsId(collaborator.Id);

                CollaboratorsDTO collaboratorDTO = ToDTO(collaborator);

                collaboratorDTO.LabelsId = labelsId;

                listCollaboratorsDTO.Add(collaboratorDTO);
            }

            totalNo = listCollaboratorsDTO.Count();

            return listCollaboratorsDTO
                .Skip((filters.PageNo - 1) * filters.PageSize)
                .Take(filters.PageSize)
                .ToList();
        }

        public void Delete(int id)
        {
            var collaborator = collaboratorsRepository.GetById(id);
            if (collaborator == null) return;

            collaboratorsRepository.Delete(collaborator);
        }
    }
}
