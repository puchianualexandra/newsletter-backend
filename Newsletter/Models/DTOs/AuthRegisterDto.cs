﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Newsletter.Models.DTOs
{
    public class AuthRegisterDto
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Password { get; set; }
        [Required]
        public string Email { get; set; }

        public string PhoneNumber { get; set; }
        public DateTime DateOfJoin { get; set; }
        public string Role { get; set; }
        public bool IsNew { get; set; }
    }
}
