﻿using Newsletter.DataLayer.Entities;
using System;

namespace Newsletter.Models.DTOs
{
    public class AuthStatusDto
    {
        public bool IsSuccessful { get; set; }
        public string Message { get; set; }
        public string Token { get; set; }
        public ResponseAdminDTO User { get; set; }
    }
}
