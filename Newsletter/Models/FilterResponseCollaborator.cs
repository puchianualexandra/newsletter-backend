﻿using Newsletter.DataLayer.Entities;
using Newsletter.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Models
{
    public class FilterResponseCollaborator
    {
        public int TotalNo { get; set; }
        public List<CollaboratorsDTO> Collaborators { get; set; }

        public FilterResponseCollaborator(int totalNo, List<CollaboratorsDTO> collaborators)
        {
            this.TotalNo = totalNo;
            this.Collaborators = collaborators;
        }
    }
}
