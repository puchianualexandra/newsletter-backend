﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newsletter.DataLayer.Entities;
using Newsletter.Models.DTOs;
using Newsletter.Repositories;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace Core.Managers
{
    public class AuthService
    {
        private IConfiguration configuration { get; set; }
        private AdminsRepository adminsRepository { get; set; }

        public AuthService(IConfiguration configuration, AdminsRepository adminsRepository)
        {
            this.configuration = configuration;
            this.adminsRepository = adminsRepository;
        }

        public AuthStatusDto Login(AuthLoginDto payload)
        {
            var entity = adminsRepository.FindByEmail(payload.Email);

            if (entity == null)
                return new AuthStatusDto
                {
                    IsSuccessful = false,
                    Message = "Username or password is wrong"
                };

            if (!(HashPassword(payload.Password, Convert.FromBase64String(entity.PasswordSalt)) == entity.PasswordHash))
                return new AuthStatusDto
                {
                    IsSuccessful = false,
                    Message = "Username or password is wrong"
                };

            var token = GenerateJwtToken(entity.Id);
            ResponseAdminDTO responseAdmin = AdminToResponseDTO(entity);

            return new AuthStatusDto
            {
                IsSuccessful = true,
                Message = "Successfully authenticated",
                User = responseAdmin,
                Token = token
            }; 
        }

        public AuthStatusDto Register(AuthRegisterDto payload)
        {
            var entity = adminsRepository.FindByEmail(payload.Email);

            if (entity != null)
                return new AuthStatusDto
                {
                    IsSuccessful = false,
                    Message = $"Email already exists"
                };

            var newAdmin = ToModel(payload);
            adminsRepository.Add(newAdmin);

            var token = GenerateJwtToken(newAdmin.Id);
            ResponseAdminDTO responseAdmin = AdminToResponseDTO(newAdmin);

            return new AuthStatusDto
            {
                IsSuccessful = true,
                Message = "Successfully registered",
                User = responseAdmin,
                Token = token
            };
        }

        public void UpdatePassword(Admin newAdmin, string password)
        {
            var salt = GenerateSalt();
            newAdmin.PasswordSalt = Convert.ToBase64String(salt);
            newAdmin.PasswordHash = HashPassword(password, salt);
            adminsRepository.Update(newAdmin);
        }

        private Admin ToModel(AuthRegisterDto payload)
        {
            var salt = GenerateSalt();
            var newAdmin = new Admin
            {
                FirstName = payload.FirstName,
                LastName = payload.LastName,
                Email = payload.Email,
                PhoneNumber= payload.PhoneNumber,
                DateOfJoin= payload.DateOfJoin,
                Role=payload.Role,
                IsNew=payload.IsNew,

                PasswordSalt = Convert.ToBase64String(salt),
                PasswordHash = HashPassword(payload.Password, salt),
            };

            return newAdmin;
        }

        private ResponseAdminDTO AdminToResponseDTO(Admin payload)
        {
            var newAdmin = new ResponseAdminDTO
            {
                Id=payload.Id,
                FirstName = payload.FirstName,
                LastName = payload.LastName,
                Email = payload.Email,
                PhoneNumber = payload.PhoneNumber,
                DateOfJoin = payload.DateOfJoin,
                Role = payload.Role,
                IsNew = payload.IsNew,
            };

            return newAdmin;
        }

        private string HashPassword(string password, byte[] salt)
        {
            var bytes = KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100000,
                numBytesRequested: 256 / 8);

            return Convert.ToBase64String(bytes);
        }

        private byte[] GenerateSalt()
        {
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            return salt;
        }

        private string GenerateJwtToken(int adminId)
        {
            var basicUserClaims = new List<Claim>()
            {
                new Claim("AdminId", adminId.ToString())
            };

            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtSettings:Secret"]));
            var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: configuration["JwtSettings:Issuer"],
                audience: configuration["JwtSettings:Audience"],
                claims: basicUserClaims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: signinCredentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}