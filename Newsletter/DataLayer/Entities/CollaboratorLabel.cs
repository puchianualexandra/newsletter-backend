﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.DataLayer.Entities
{
    public class CollaboratorLabel
    {
        [Key]
        public int CollaboratorId { get; set; }
       
        public virtual Collaborator Collaborator { get; set; }
        
        [Key]
        public int LabelId { get; set; }
        
        public virtual Label Label { get; set; }
    }
}
