﻿using Newsletter.DataLayer;
using Newsletter.DataLayer.Entities;
using Newsletter.Models;
using Newsletter.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.Repositories
{
    public class ArticlesRepository
    {
        DatabaseContext databaseContext { get; set; }

        public ArticlesRepository(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public List<Article> GetAll()
        {
            var results = databaseContext.Articles.ToList();

            return results;
        }
        
        public List<Article> GetAllPosted()
        {
            var results = databaseContext.Articles.Where(a => a.IsPosted == true).ToList();

            return results;
        }

        public void Add(Article article)
        {
            databaseContext.Articles.AddRange(article);
            databaseContext.SaveChanges();
        }

        public void Update(Article updatedArticle)
        {
            databaseContext.Articles.Update(updatedArticle);
            databaseContext.SaveChanges();
        }
        
        public void UpdateIsPosted(Article updatedArticle)
        {
            databaseContext.Articles.Update(updatedArticle);
            databaseContext.SaveChanges();
        }

        public Article GetById(int id)
        {
            var article = databaseContext.Find<Article>(id);
            return article;
        }

        public List<Article> GetByCollaboratorId(int id)
        {
            var results = databaseContext.Articles
                .Where(a => a.CollaboratorId == id)
                .ToList();
            return results;
        }

        public List<Article> ContainsFiltersUser(UserFilters filters)
        {
            var results = databaseContext.Articles
                .Where(a => a.Title.Contains(filters.Title) && a.SpecialSection.Contains(filters.SpecialSection) && a.IsPosted == true)
                .ToList();

            return results;
        }
        
        public List<Article> ContainsFiltersAdmin(AdminFilters filters)
        {
            var results = databaseContext.Articles
                .Where(a => a.Title.Contains(filters.Title))
                .ToList();

            return results;
        }

        public void Delete(Article article)
        {
            databaseContext.Remove<Article>(article);
            databaseContext.SaveChanges();
        }
    }
}
