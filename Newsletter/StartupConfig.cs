﻿using Core.Managers;
using Microsoft.Extensions.DependencyInjection;
using Newsletter.Repositories;
using Newsletter.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter
{
    public static class StartupConfig
    {
        public static void AddServices(this IServiceCollection services)
        {
            services.AddScoped<AdminsService>();
            services.AddScoped<SubscribersService>();
            services.AddScoped<NewsletterEditionsService>();
            services.AddScoped<LabelsService>();
            services.AddScoped<SuggestionsService>();
            services.AddScoped<CollaboratorsService>();
            services.AddScoped<CollaboratorLabelsService>();
            services.AddScoped<FacultiesService>();
            services.AddScoped<AuthService>();
            services.AddScoped<ArticlesService>();
            services.AddScoped<ArticleItemsService>();

        }

        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<AdminsRepository>();
            services.AddScoped<SubscribersRepository>();
            services.AddScoped<NewsletterEditionsRepository>();
            services.AddScoped<LabelsRepository>();
            services.AddScoped<SuggestionsRepository>();
            services.AddScoped<CollaboratorsRepository>();
            services.AddScoped<CollaboratorLabelsRepository>();
            services.AddScoped<FacultiesRepository>();
            services.AddScoped<ArticlesRepository>();
            services.AddScoped<ArticleItemsRepository>();
        }
    }
}
