﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newsletter.DataLayer.Entities
{
    public class Article
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime DateOfUpload { get; set; }
        public int? CollaboratorId { get; set; }
        public virtual Collaborator Collaborator { get; set; }
        public string Description { get; set; }
        public bool IsPosted { get; set; }
        public string SpecialSection { get; set; }
        public List<ArticleItem> ArticleItems { get; set; }
    }
}
