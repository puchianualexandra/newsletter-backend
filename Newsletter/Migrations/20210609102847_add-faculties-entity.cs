﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Newsletter.Migrations
{
    public partial class addfacultiesentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Faculty",
                table: "Collaborators");

            migrationBuilder.AddColumn<int>(
                name: "FacultyId",
                table: "Collaborators",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Faculties",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Faculties", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Collaborators_FacultyId",
                table: "Collaborators",
                column: "FacultyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Collaborators_Faculties_FacultyId",
                table: "Collaborators",
                column: "FacultyId",
                principalTable: "Faculties",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Collaborators_Faculties_FacultyId",
                table: "Collaborators");

            migrationBuilder.DropTable(
                name: "Faculties");

            migrationBuilder.DropIndex(
                name: "IX_Collaborators_FacultyId",
                table: "Collaborators");

            migrationBuilder.DropColumn(
                name: "FacultyId",
                table: "Collaborators");

            migrationBuilder.AddColumn<string>(
                name: "Faculty",
                table: "Collaborators",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
