﻿using Microsoft.AspNetCore.Mvc;
using Newsletter.DataLayer.Entities;
using Newsletter.Models.DTOs;
using Newsletter.Services;

namespace Newsletter.Controllers
{
    [ApiController]
    [Route("labels")]
    public class LabelsController : ControllerBase
    {
        LabelsService labelsService;

        public LabelsController(LabelsService labelsService)
        {
            this.labelsService = labelsService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var results = labelsService.GetAll();

            if (results == null)
                return BadRequest();

            return Ok(results);
        }

        [HttpGet("byId/{id}")]
        public IActionResult GetById(int id)
        {
            var result = labelsService.GetById(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpPost]
        public IActionResult Post([FromBody] LabelsDTO labelsDTO)
        {
            labelsService.Add(labelsDTO);

            return Ok();
        }

        [HttpPut("{id}")]
        public void Put([FromRoute] int id, [FromBody] LabelsDTO labelDTO)
        {
            labelDTO.Id = id;

            labelsService.Update(labelDTO);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            labelsService.Delete(id);
        }
    }
}
